
  <?php include('includes/imagepath.php'); ?>

  <?php include('includes/html_header.php'); ?>

  <?php include('includes/template_navigation.php'); ?>

  <?php include('includes/header.php'); ?>



  <!-- MAIN-CONTENT + SIDEBAR -->

<div class="ew-fullwidth cust-fullwidth main-content">

  <div class="container">

    <div class="row">

      <div class="col-md-8">

        <div class="ew-main-content cust-main-content">

          <h1>Lorem ipsum dolor sit amet consetetur sadipscing elitr</h1>

          <h2>Consectetur adipisicing elit</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>

          <blockquote>
            Lorem ipsum dolor sit amet, consectetuer
            adipiscing elit. Aenean commodo ligula eget dolor.
            Aenean massa <strong>strong</strong>. Cum sociis
            aliquet nec, vulputate eget, arcu. In <em>em</em>
            enim justo, rhoncus ut, imperdiet a, venenatis vitae,
            justo. Nullam <a class="external ext" href="#">link</a>
            dictum felis eu pede mollis pretium.
          </blockquote>


          <h2>Voluptas aliquam ut qui tempore numquam a eveniet</h2>
          <p>Lorem ipsum dolor sit amet, <a href="xxx">consectetur adipisicing elit</a>. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>

          <table class="table ew-table cust-table table-striped ew-table-striped cust-table-striped">
            <tr>
              <th>Entry Header 1</th>
              <th>Entry Header 2</th>
              <th>Entry Header 3</th>
              <th>Entry Header 4</th>
            </tr>
            <tr>
              <td>Entry First Line 1</td>
              <td>Entry First Line 2</td>
              <td>Entry First Line 3</td>
              <td>Entry First Line 4</td>
            </tr>
            <tr>
              <td>Entry Line 1</td>
              <td>Entry Line 2</td>
              <td>Entry Line 3</td>
              <td>Entry Line 4</td>
            </tr>
            <tr>
              <td>Entry Last Line 1</td>
              <td>Entry Last Line 2</td>
              <td>Entry Last Line 3</td>
              <td>Entry Last Line 4</td>
            </tr>
          </table>

          <h2>Commodi maxime, optio? Lorem ipsum dolor sit amet</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>

          <ul>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing
              elit. Aenean commodo ligula eget dolor. Aenean
              massa.</li>
              <li>Cum sociis natoque penatibus et magnis dis
                parturient montes, nascetur ridiculus mus. Donec quam
                felis, ultricies nec, pellentesque eu, pretium quis,
                sem.</li>
                <li>Nulla consequat massa quis enim. Donec pede justo,
                  fringilla vel, aliquet nec, vulputate eget, arcu.</li>
                  <li>In enim justo, rhoncus ut, imperdiet a, venenatis
                    vitae, justo. Nullam dictum felis eu pede mollis
                    pretium. Integer tincidunt.</li>
                  </ul>

                  <h3>Voluptas aliquam ut qui tempore numquam a eveniet</h3>
                  <p>Lorem ipsum dolor sit amet, <a href="xxx">consectetur adipisicing elit</a>. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>

                  <h4>Voluptas aliquam ut qui tempore numquam a eveniet</h4>
                  <p>Lorem ipsum dolor sit amet, <a href="xxx">consectetur adipisicing elit</a>. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio? Lorem ipsum d Commodi maxime, optio?</p>

                </div> <!-- ew-main-content cust-main-content -->

              </div> <!-- col-md-8 -->

              <div class="col-md-4">

                <div class="ew-sidebar cust-sidebar">

                  <div class="ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-one">
                    <div class="ew-sidebar-box cust-sidebar-box">

                      <img src="<?php echo $imagePath; ?>sidebar-01.jpg" class="img-responsive" alt="xxx">

                      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                      <p>Amet fuga laboriosam, cum voluptatum qui, veniam eligendi, inventore odio dolores officia atque excepturi, quibusdam libero? Quam doloremque cupiditate eveniet ad, cumque.</p>
                      <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default simple">weiter lesen</a></p>

                    </div> <!-- ew-sidebar-box cust-sidebar-box -->
                  </div> <!-- ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-one -->

                  <div class="ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-two">
                    <div class="ew-sidebar-box cust-sidebar-box">

                      <img src="<?php echo $imagePath; ?>sidebar-02.jpg" class="img-responsive" alt="xxx">

                      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                      <p>Amet fuga laboriosam, cum voluptatum qui, veniam eligendi, inventore odio dolores officia atque excepturi, quibusdam libero? Quam doloremque cupiditate eveniet ad, cumque.</p>
                      <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default border">weiter lesen</a></p>

                    </div> <!-- ew-sidebar-box cust-sidebar-box -->
                  </div> <!-- ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-two -->

                  <div class="ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-three">
                    <div class="ew-sidebar-box cust-sidebar-box">

                      <img src="<?php echo $imagePath; ?>sidebar-03.jpg" class="img-responsive" alt="xxx">

                      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                      <p>Amet fuga laboriosam, cum voluptatum qui, veniam eligendi, inventore odio dolores officia atque excepturi, quibusdam libero? Quam doloremque cupiditate eveniet ad, cumque.</p>
                      <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default simple">weiter lesen</a></p>

                    </div> <!-- ew-sidebar-box cust-sidebar-box -->
                  </div> <!-- ew-sidebar-box-wrapper ew-sidebar-box-wrapper style-three -->

                </div> <!-- col-md-4 -->

              </div> <!-- ew-sidebar cust-sidebar -->

            </div> <!-- row -->

          </div> <!-- container -->

</div> <!-- ew-fullwidth cust-fullwidth main-content -->

          <!-- END MAIN-CONTENT + SIDEBAR -->



  <?php include('includes/footer.php'); ?>

  <?php include('includes/js.php'); ?>

  </body>
</html>
