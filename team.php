
  <?php include('includes/imagepath.php'); ?>

  <?php include('includes/html_header.php'); ?>

  <?php include('includes/template_navigation.php'); ?>

  <?php include('includes/header.php'); ?>



  <div class="ew-fullwidth cust-fullwidth main-content-full page-team">

    <div class="container">

      <div class="ew-listing-page cust-listing-page">

        <div class="row">
          <div class="col-md-6">
          <h1>Unsere Mitarbeiterinnen und Mitarbeiter</h1>
          </div> <!-- col -->
          <div class="col-md-6">
          </div> <!-- col -->
        </div> <!-- row -->

        <div class="row">
            <div class="col-md-6">
                <p><strong>Nur Dank dem Einsatz unserer Mitarbeiterinnen und Mitarbeiter in der Schweiz und vor Ort in Rio können wir unsere hochgesteckten Ziele erreichen. Folgende Personen engagieren sich aus Überzeugung und mit viel Herzblut für FuturoGoal.</strong></p>
              </div> <!-- col -->
            <div class="col-md-6">
            </div> <!-- col -->
          </div> <!-- row -->

        </div> <!-- ew-listing-page cust-listing-page #node -->

        <div class="row">
          <div class="col-md-12">
            <h2>Administration</h2>
          </div> <!-- col -->
        </div> <!-- row -->

        <div class="row">

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Paula Abdul" title="Paula Abdul" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-paula-abdul.jpg?itok=7_vJS4hF">    <h3>Paula Abdul</h3>
            <h4>Office Managerin</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 11</div>
              <div>F +41 41 760 22 41</div>
              <a href="mailto:paula.abdul@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Terence Hill" title="Terence Hill" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-terence-hill.jpg?itok=GuPGOOhc">   <h3>Terence Hill</h3>
            <h4>Office Manager</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 47</div>
              <div>F +41 41 720 22 41</div>
              <a href="mailto:terence.hill@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Steven Seagal" title="Steven Seagal" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-steven-seagal.jpg?itok=b9OX3E6v">    <h3>Steven Seagal</h3>
            <h4>Office Manager</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 33 44</div>
              <div>F +41 41 760 22 41</div>
              <a href="mailto:steven.seagal@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Uma Thurman" title="Uma Thurman" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-uma-thurman.jpg?itok=EySACRHj">    <h3>Uma Thurman</h3>
            <h4>Office Managerin</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 17</div>
              <div>F +41 41 760 22 38</div>
              <a href="mailto:uma.thurman@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

        </div> <!-- row -->

        <div class="row">
          <div class="col-md-12">
            <h2>Projektteam</h2>
          </div> <!-- col -->
        </div> <!-- row -->

        <div class="row">

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Jenifer Aniston" title="Jenifer Aniston" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-jenifer-aniston_0.jpg?itok=hGAj2g18">    <h3>Jenifer Aniston</h3>
            <h4>Trainerin</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 11</div>
              <div>F +41 41 760 22 41</div>
              <a href="mailto:jenifer.aniston@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Monica Bellucci" title="Monica Bellucci" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-monica-bellucci.jpg?itok=vyiu_t15">    <h3>Monica Bellucci</h3>
            <h4>Trainerin</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 11</div>
              <div>F +41 41 760 20 11</div>
              <a href="mailto:monica.bellucci@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Chuck Norris" title="Chuck Norris" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-chuck-norris.jpg?itok=meQqC37d">   <h3>Chuck Norris</h3>
            <h4>Trainer</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 33 44</div>
              <div>F +41 51 260 33 44</div>
              <a href="mailto:chuck.norris@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Jean-Claude Van Damme" title="Jean-Claude Van Damme" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-jean-claude-van-damme.jpeg?itok=9OEIHCI4">   <h3>Jean-Claude Van Damme</h3>
            <h4>Trainer</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 44</div>
              <div>F +41 41 760 22 41</div>
              <a href="mailto:jean.claude.van.damme@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

        </div> <!-- row -->

        <div class="row">
          <div class="col-md-12">
            <h2>Vorstand</h2>
          </div> <!-- col -->
        </div> <!-- row -->

        <div class="row">

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Adriano Celentano" title="Adriano Celentano" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-a-celentano.jpg?itok=feTd0sD_">    <h3>Adriano Celentano</h3>
            <h4>CEO</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 44</div>
              <div>F +41 41 760 22 41</div>
              <a href="mailto:adriano.celentano@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

          <div class="col-sm-6 col-md-3 ew-team cust-team">
            <img alt="Bud Spencer" title="Bud Spencer" class="responsive-basic img-responsive" src="<?php echo $imagePath; ?>xxx-team-bud-spencer.jpg?itok=_tJeF1bT">    <h3>Bud Spencer</h3>
            <h4>CFO</h4>
            <div class="ew-team-contact cust-team-contact">
              <div>T +41 41 760 22 11</div>
              <div>F +41 41 760 20 11</div>
              <a href="mailto:bud.spencer@futurogoal.com">E-Mail</a>
            </div> <!-- ew-team-contact cust-team-contact -->
          </div> <!-- ew-team cust-team -->

        </div> <!-- row -->

      </div> <!-- container -->

    </div> <!-- ew-fullwidth cust-fullwidth main-content-full -->

  <?php include('includes/footer.php'); ?>

  <?php include('includes/js.php'); ?>

  </body>
</html>
