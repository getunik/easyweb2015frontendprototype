#getunikfrontendboilerplate

## Create new bitbucket repo for "yourprojectname"
Create a new bitbucket repo for your project and write down the url

## Clone repository
```
$ git clone git@bitbucket.org:getunik/getunikfrontendboilerplate.git yourprojectname
```

## Install node modules for grunt

```
$ npm install
```

## Run Project Setup

```
$ grunt setup
```
You will need to enter a project name and the new GIT remote origin url, so please create a new repo for your project first and write down the url.
