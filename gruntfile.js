    module.exports = function(grunt) {

        // leverage nodes path module, for finding out current file path and add it to grunt.config
        var path = require('path'),
            debug = true;

        // set stuff
        grunt.config.set('currentFilePath', path.resolve());

        //TODO: deployment server settings should be prompted on setup and then saved and read later from a json file
        grunt.config.set('deployUrlBase', '/home/getunik/www/dev.getunik.net/test/base/');
        grunt.config.set('deployUrlCustomer', '/home/getunik/www/dev.getunik.net/test/customer/');

        // debug output
        if (debug) {
            grunt.log.writeln('');
            grunt.log.writeln('currentFilePath: ');
            grunt.log.writeln(grunt.config("currentFilePath"));

            grunt.log.writeln('');
            grunt.log.writeln('deployUrlBase: ');
            grunt.log.writeln(grunt.config("deployUrlBase"));

            grunt.log.writeln('');
            grunt.log.writeln('deployUrlCustomer: ');
            grunt.log.writeln(grunt.config("deployUrlCustomer"));
        }


        grunt.config.init({
            prompt: {
                projectname: {
                    options: {
                        questions: [
                            {
                                config: 'projectname', // arbitrary name or config for any other grunt task
                                type: 'input', // list, checkbox, confirm, input, password
                                message: 'What is the projects name?', // Question to ask the user, function needs to return a string,
                                default: '%%%projectname%%%' // default value if nothing is entered
                            }
                        ]
                    }
                },
                gitremoteurl: {
                    options: {
                        questions: [
                            {
                                config: 'gitremoteurl', // arbitrary name or config for any other grunt task
                                type: 'input', // list, checkbox, confirm, input, password
                                message: 'What is the projects git remote origin url?'
                            }
                        ]
                    }
                }
            },
            shell: {
                git_submodule_update: {
                    command: [
                        'echo updating git submodules...',
                        'git submodule update --init --recursive'
                    ].join('&&')
                },
                git_remote_url: {
                    command: [
                        'echo changing git remote origin url to: <%=grunt.config("gitremoteurl")%>...',
                        'git remote set-url origin <%=grunt.config("gitremoteurl")%>',
                        'git remote -v'
                    ].join('&&')
                }
            },
            notify_hooks: {
                options: {
                  enabled: true,
                  max_jshint_notifications: 5, // maximum number of notifications from jshint output
                  success: true, // whether successful grunt executions should be notified automatically
                  duration: 3 // the duration of notification in seconds, for `notify-send only
                }
            },
            'sftp-deploy': {
              base: {
                auth: {
                  host: 'guroot.nine.ch',
                  port: 22,
                  authKey: 'key1'
                },
                cache: false,
                src: path.resolve(),
                dest: grunt.config("deployUrlBase"),
                exclusions: [
                    '.DS_Store',
                    '.ftppass',
                    '.git',
                    '.gitignore',
                    '.gitmodules',
                    'sftpCache.json',
                    'bootstrap',
                    'getunikpreprocessormixins',
                    'less',
                    'node_modules',
                    'gruntfile.js',
                    'package.json',
                    'readme.md'
                    ],
                serverSep: '/',
                concurrency: 4,
                progress: true
              },
              customer: {
                auth: {
                  host: 'guroot.nine.ch',
                  port: 22,
                  authKey: 'key1'
                },
                cache: false,
                src: path.resolve(),
                dest: grunt.config("deployUrlCustomer"),
                exclusions: [
                    '.DS_Store',
                    '.ftppass',
                    '.git',
                    '.gitignore',
                    '.gitmodules',
                    'sftpCache.json',
                    'bootstrap',
                    'getunikpreprocessormixins',
                    'less',
                    'node_modules',
                    'gruntfile.js',
                    'package.json',
                    'readme.md'
                    ],
                serverSep: '/',
                concurrency: 4,
                progress: true
              }
            },
            replace: {
                base: {
                    options: {
                        patterns: [
                            {
                                match: '$mode = \'customer\'',
                                replacement: '$mode = \'base\''
                            }
                        ],
                        usePrefix: false
                    },
                    files: [
                        {expand: true, flatten: true, src: ['includes/imagepath.php'], dest: 'includes/'}
                    ]
                },
                customer: {
                    options: {
                        patterns: [
                            {
                                match: '$mode = \'base\'',
                                replacement: '$mode = \'customer\''
                            }
                        ],
                        usePrefix: false
                    },
                    files: [
                        {expand: true, flatten: true, src: ['includes/imagepath.php'], dest: 'includes/'}
                    ]
                },
                projectname: {
                    options: {
                        patterns: [
                            {
                                match: '%%%projectname%%%',
                                replacement: '<%=grunt.config("projectname")%>'
                            }
                        ],
                        usePrefix: false
                    },
                    files: [
                        {expand: true, flatten: true, src: ['includes/html_header.php'], dest: 'includes/'}
                    ]
                }
            },
            less: {
                base: {
                    options: {
                        cleancss: true
                    },
                    files: {
                        "css/styles.css": "less/base/styles.less"
                    }
                },
                customer: {
                    options: {
                        cleancss: true
                    },
                    files: {
                        "css/styles.css": "less/customer/styles.less"
                    }
                }
            },
            watch: {
                base: {
                    files: ['less/base/*.less'], // which files to watch
                    tasks: ['less:base', 'replace:base']
                },
                customer: {
                    files: ['less/customer/*.less'], // which files to watch
                    tasks: ['less:customer', 'replace:customer']
                }
            }
        });

        // load modules
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-notify');
        grunt.loadNpmTasks('grunt-replace');
        grunt.loadNpmTasks('grunt-sftp-deploy');
        grunt.loadNpmTasks('grunt-shell');
        grunt.loadNpmTasks('grunt-prompt');

        // default watch task
        grunt.registerTask('default', ['watch']);

        // deployment
        grunt.registerTask('deploybase', ['less:base', 'replace:base', 'sftp-deploy:base']);
        grunt.registerTask('deploycustomer', ['less:customer', 'replace:customer', 'sftp-deploy:customer']);
        grunt.registerTask('deploy', ['deploybase', 'deploycustomer']);

        // setup
        grunt.registerTask('setup', [
            'prompt:projectname',
            'prompt:gitremoteurl',
            'replace:projectname',
            'shell:git_remote_url',
            'shell:git_submodule_update'
        ]);

        // notify
        grunt.task.run('notify_hooks');
    };
