
<!-- FOOTER SITEMAP -->
<div class="ew-fullwidth cust-fullwidth footer-sitemap">

	<div class="container visible-xs">
		<div class="row">
			<div class="col-xs-12">
				<a href="xxx" class="ew-sitemap-link cust-sitemap-link text-center">Sitemap</a>
			</div> <!-- col-xs-12 -->
		</div> <!-- row -->
	</div> <!-- container -->

<div class="ew-fullwidth cust-fullwidth bottom">

	<div class="container hidden-xs ew-footer-sitemap cust-footer-sitemap">
		<div class="row">

			<div class="col-sm-4 col-md-2">
				<h4>Über uns</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">Was wir tun</a>
					</li>
					<li><a href="xxx">Wer wir sind</a>
					</li>
					<li><a href="xxx">Geschichte</a>
					</li>
					<li><a href="xxx">Kinderrechte</a>
					</li>
					<li><a href="xxx">Meilensteine</a>
					</li>
					<li><a href="xxx">Kontakt</a>
					</li>
					<li><a href="xxx">Fragen & Antworten</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

			<div class="col-sm-4 col-md-2">
				<h4>Schwerpunkte</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">Überleben und</a>
					</li>
					<li><a href="xxx">Schule und Bildung</a>
					</li>
					<li><a href="xxx">Schutz </a>
					</li>
					<li><a href="xxx">Nothilfe</a>
					</li>
					<li><a href="xxx">Advocacy</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

			<div class="col-sm-4 col-md-2">
				<h4>Projekte</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">Projekte weltweit</a>
					</li>
					<li><a href="xxx">Projekte schweizweit</a>
					</li>
					<li><a href="xxx">News zu den Projekten </a>
					</li>
					<li><a href="xxx">Magic13</a>
					</li>
					<li><a href="xxx">Netzwerk</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

			<!-- Add the extra clearfix for only the required viewport -->
			<div class="clearfix visible-sm"></div>

			<div class="col-sm-4 col-md-2">
				<h4>Partner</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">Unsere Partner</a>
					</li>
					<li><a href="xxx">Kontakt</a>
					</li>
					<li><a href="xxx">Partner werden</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

			<div class="col-sm-4 col-md-2">
				<h4>News & Media</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">News</a>
					</li>
					<li><a href="xxx">Publikationen</a>
					</li>
					<li><a href="xxx">Facebook & Twitter</a>
					</li>
					<li><a href="xxx">Newsletter</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

			<div class="col-sm-4 col-md-2">
				<h4>Spenden</h4>
				<ul class="ew-footer-sitemap cust-footer-sitemap">
					<li><a href="xxx">Einmalige Spende</a>
					</li>
					<li><a href="xxx">Regelmässige Spende</a>
					</li>
					<li><a href="xxx">Aktiv sein</a>
					</li>
					<li><a href="xxx">Unterlagen anfordern</a>
					</li>
					<li><a href="xxx">Artikel kaufen</a>
					</li>
					<li><a href="xxx">Sammelaktionen</a>
					</li>
					<li><a href="xxx">Transparent und nachprüfbar</a>
					</li>
				</ul>
			</div>
			<!-- col-md-2 -->

		</div>
		<!-- row -->
	</div>
	<!-- container -->
</div> <!-- ew-fullwidth cust-fullwidth footer-sitemap -->
<!-- END FOOTER SITEMAP -->

</div> <!-- ew-fullwidth cust-fullwidth bottom -->

<!-- FOOTER -->
<div class="ew-fullwidth cust-fullwidth footer">
	<div class="container ew-container cust-container footer">
		<div class="ew-footer-wrapper cust-footer-wrapper">
			<div class="row ew-row cust-row footer">

				<div class="col-sm-3 col-md-3 col-lg-2">
					<div class="ew-footer-col cust-footer-col one">
						<h4>Rechtliches</h4>
						<ul class="ew-footer cust-footer servicenavi">
							<li><a href="xxx">FAQ</a>
							</li>
							<li><a href="xxx">IMPRESSUM</a>
							</li>
							<li><a href="xxx">AGBS UND DATENSCHUTZ</a>
							</li>
						</ul> <!-- ew-footer-servicenavi cust-footer-servicenavi -->
					</div> <!-- ew-footer-col cust-footer-col one -->
				</div> <!-- col -->

				<div class="col-sm-3 col-md-3 col-lg-2">
					<div class="ew-footer-col cust-footer-col two">
						<h4>Kontakt</h4>
						<p>
							HILFSWERK
							<br />Hardturmstrasse 101
							<br />8005 Zürich
							<br />
							<br />Telefon: +41 44 267 74 70
							<br />Fax: +41 44 267 74 98
							<br />E-Mail: info@hilfswerk.ch
						</p>

						<ul class="ew-social-media cust-social-media visible-xs visible-sm">
							<li><a target="_blank" href="xxx"><i class="fa fa-envelope"></i></a>
							</li>
							<li><a target="_blank" href="xxx"><i class="fa fa-twitter"></i></a>
							</li>
							<li><a target="_blank" href="xxx"><i class="fa fa-facebook"></i></a>
							</li>
						</ul>
						<!-- social-media -->

					</div> <!-- ew-footer-col cust-footer-col two -->
				</div> <!-- col -->

				<div class="col-sm-6 col-md-6 col-lg-8">
					<div class="ew-footer-col cust-footer-col three">
						<h4>Kontoverbindungen</h4>
						<p>
							Post: 80-15233-8
							<br />
							Bank: CH88 0900 0000 8001 5233 8</p>

						</div> <!-- ew-footer-col cust-footer-col three -->
					</div> <!-- col -->

				</div>
				<!-- row -->

			</div> <!-- ew-footer-wrapper cust-footer-wrapper -->
		</div>
		<!-- container -->
	</div> <!-- ew-fullwidth cust-fullwidth footer -->
	<!-- END FOOTER -->

