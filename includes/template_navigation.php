  <div class="container">

    <div class="row">
      <div class="col-xs-12">

		<div class="btn-group pull-right" role="group" aria-label="btn-group">
			<a href="index.php" class="btn btn-xs btn-default">Home</a>
			<a href="content-page.php" class="btn btn-xs btn-default">Content page</a>
			<a href="news.php" class="btn btn-xs btn-default">News</a>
			<a href="team.php" class="btn btn-xs btn-default">Team</a>
		</div> <!-- /.btn-group -->

      </div><!-- /.col -->
    </div><!-- /.row -->

  </div><!-- /.container -->
