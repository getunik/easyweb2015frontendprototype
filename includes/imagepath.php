
<?php

  // set 'base' || customer
  $mode = 'base';

  $config = array(
    'base' => array(
      'imagePath' => 'images/base/'
    ),
    'customer' => array(
      'imagePath' => 'images/customer/'
    ),
  );

  $imagePath = $config[$mode]['imagePath'];

?>
