<!-- HEADER -->
<div class="ew-fullwidth cust-fullwidth header">
	<div class="container">
		<div class="row">

			<div class="col-sm-4 col-md-5">
				<a class="logo hidden-xs" href="index.php">
					<img src="<?php echo $imagePath; ?>futurogoal-logo.png" alt="Futuro Goal">
				</a>

				<ul class="ew-langswitcher cust-langswitcher visible-xs">
					<li><a class="active" href="xxx">DE</a>
					</li>
					<li><a href="xxx">FR</a>
					</li>
					<li><a href="xxx">IT</a>
					</li>
				</ul>
				<!-- langswitcher -->

				<button type="button" class="btn btn-default ew-btn-default cust-btn-default ew-donate-now cust-donate-now visible-xs">
					Sofort Spenden
				</button>
				<!-- donate now -->
			</div>
			<!-- col-md-5 -->

			<div class="col-md-1 ew-no-padding-lr cust-no-padding-lr">
				<ul class="ew-langswitcher cust-langswitcher hidden-xs">
					<li><a class="active" href="xxx">DE</a>
					</li>
					<li><a href="xxx">FR</a>
					</li>
					<li><a href="xxx">IT</a>
					</li>
				</ul>
				<!-- langswitcher -->
			</div>
			<!-- col-md-2 -->

			<div class="col-md-3">
				<form class="navbar-form ew-navbar-form cust-navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control ew-form-control cust-form-control" placeholder="Search" name="srch-term" id="srch-term">
						<div class="input-group-btn ew-input-group-btn cust-input-group-btn">
							<button class="btn btn-default ew-btn-default cust-btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
						<!-- input-group-btn -->
					</div>
					<!-- input-group -->
				</form>
				<!-- navbar-form -->
			</div>
			<!-- col-md-2 -->

			<div class="col-md-1 ew-no-padding-lr cust-no-padding-lr">
				<ul class="ew-social-media cust-social-media hidden-xs hidden-sm">
					<li><a target="_blank" href="xxx"><i class="fa fa-envelope"></i></a>
					</li>
					<li><a target="_blank" href="xxx"><i class="fa fa-twitter"></i></a>
					</li>
					<li><a target="_blank" href="xxx"><i class="fa fa-facebook"></i></a>
					</li>
				</ul>
				<!-- social-media -->
			</div>
			<!-- col-md-2 -->

			<div class="col-md-2">
				<button type="button" class="btn btn-default ew-btn-default cust-btn-default ew-donate-now cust-donate-now hidden-xs">
					Sofort Spenden
				</button>
				<!-- donate now -->
			</div>
			<!-- col-md-2 -->

		</div>
		<!-- row -->
	</div>
	<!-- container -->
</div>
<!-- ew-fullwidth cust-fullwidth header -->
<!-- END HEADER -->

<!-- NAVIGATION -->
<div class="ew-fullwidth cust-fullwidth navigation">
	<div class="container">

		<nav class="navbar navbar-default ew-navbar-default cust-navbar-default" role="navigation">
			<div class="container-fluid ew-container-fluid cust-container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header ew-navbar-header cust-navbar-header">
					<button type="button" class="navbar-toggle ew-navbar-toggle cust-navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand ew-navbar-brand cust-navbar-brand visible-xs" href="xxx">
						<img src="<?php echo $imagePath; ?>futurogoal-logo.png" alt="Futuro Goal">
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse ew-example-navbar-collapse-1 cust-example-navbar-collapse-1" id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav ew-navbar-nav cust-navbar-nav">
						<li class="active"><a href="xxx">Über uns</a>
						</li>
						<li><a href="xxx">Schwerpunkt</a>
						</li>
						<li class="dropdown">
							<a href="xxx" class="dropdown-toggle" data-toggle="dropdown">Projekte <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="xxx">Projekte weltweit</a>
								</li>
								<li><a href="xxx">Projekte schweizweit</a>
								</li>
								<li><a href="xxx">News zu den Projekten</a>
								</li>
								<li><a href="xxx">Magic13</a>
								</li>
								<li><a href="xxx">Netzwerk</a>
								</li>
							</ul>
						</li>
						<li><a href="xxx">Partner</a>
						</li>
						<li><a href="xxx">News & Media</a>
						</li>
						<li><a href="xxx">Spenden</a>
						</li>
					</ul>

				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>

	</div>
	<!-- container -->
</div> <!-- ew-fullwidth cust-fullwidth navigation -->
<!-- END NAVIGATION -->

