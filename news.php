
  <?php include('includes/imagepath.php'); ?>

  <?php include('includes/html_header.php'); ?>

  <?php include('includes/template_navigation.php'); ?>

  <?php include('includes/header.php'); ?>



  <!-- MAIN-CONTENT + SIDEBAR -->

<div class="ew-fullwidth cust-fullwidth main-content">

  <div class="container">

    <h1>News Übersicht</h1>

    <div class="row ew-row cust-row">

      <div class="col-md-9">

        <!-- NEWS ROW -->
        <div class="row ew-row cust-row news">
          <div class="col-md-8">
            <h2>Voluptas aliquam ut qui tempore numquam</h2>

            <p class="ew-news-date cust-news-date">15. November 2014</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>
            <p><a role="button" href="xxx" class="ew-btn-default cust-btn-default">weiter lesen</a>
            </div> <!-- col-md-8 -->

          <div class="col-md-4">
            <img src="<?php echo $imagePath; ?>sidebar-03.jpg" class="img-responsive" alt="xxx">
          </div>

          </div> <!-- row ew-row cust-row news -->
          <!-- END NEWS ROW -->

          <!-- NEWS ROW -->
          <div class="row ew-row cust-row news">
            <div class="col-md-8">
              <h2>Voluptas aliquam ut qui tempore numquam</h2>
              <p class="ew-news-date cust-news-date">15. November 2014</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>
              <p><a role="button" href="xxx" class="ew-btn-default cust-btn-default">weiter lesen</a>
              </div> <!-- col-md-8 -->

            <div class="col-md-4">
              <img src="<?php echo $imagePath; ?>sidebar-02.jpg" class="img-responsive" alt="xxx">
            </div>

            </div> <!-- row ew-row cust-row news -->
            <!-- END NEWS ROW -->

            <!-- NEWS ROW -->
            <div class="row ew-row cust-row news">
              <div class="col-md-8">
                <h2>Voluptas aliquam ut qui tempore numquam</h2>
                <p class="ew-news-date cust-news-date">15. November 2014</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas aliquam ut qui tempore numquam a eveniet, quis nobis inventore temporibus praesentium provident, odit quia enim cum aliquid! Commodi maxime, optio?</p>
                <p><a role="button" href="xxx" class="ew-btn-default cust-btn-default">weiter lesen</a>
                </div> <!-- col-md-8 -->

              <div class="col-md-4">
                <img src="<?php echo $imagePath; ?>sidebar-01.jpg" class="img-responsive" alt="xxx">
              </div>

              </div> <!-- row ew-row cust-row news -->
              <!-- END NEWS ROW -->

            </div> <!-- col-md-9 -->

            <div class="col-md-3">

<div class="ew-sidebar cust-sidebar news">

<div class="panel-pane pane-views-panes pane-news-category-news-category-content-pane">
<h2 class="pane-title">News Category</h2>
<div class="item-list ew-sidebar-element cust-sidebar-element news category">

<ul class="views-summary">
<li><a href="/en/news/category/10">Fundraising<span class="ew-count cust-count">3</span></a></li>
<li><a href="/en/news/category/9">Fussball<span class="ew-count cust-count">2</span></a></li>
<li><a href="/en/news/category/11">Website        <span class="ew-count cust-count">1</span></a>
</li>
</ul>
</div> <!-- item-list ew-sidebar-element cust-sidebar-element news category -->
</div> <!-- panel-pane pane-views-panes pane-news-category-news-category-content-pane-->


<div class="panel-pane pane-views-panes pane-news-archive-news-archive-content-pane">
<h2 class="pane-title">News Archive</h2>

<div class="item-list ew-sidebar-element cust-sidebar-element news archive">
<ul class="views-summary">
<li><a href="/en/news/archive/2013-01">2013-01<span class="ew-count cust-count">1</span></a></li>
<li><a href="/en/news/archive/2012-06">2012-06<span class="ew-count cust-count">1</span></a></li>
</ul>
</div> <!-- item-list ew-sidebar-element cust-sidebar-element news archive -->
</div> <!-- panel-pane pane-views-panes pane-news-archive-news-archive-content-pane -->

              </div> <!-- col-md-3 -->

            </div> <!-- ew-sidebar cust-sidebar news -->

          </div> <!-- row -->

        </div> <!-- container -->

</div> <!-- ew-fullwidth cust-fullwidth main-content -->

        <!-- END MAIN-CONTENT + SIDEBAR -->



  <?php include('includes/footer.php'); ?>

  <?php include('includes/js.php'); ?>

  </body>
</html>
