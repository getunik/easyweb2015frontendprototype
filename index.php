
  <?php include('includes/imagepath.php'); ?>

  <?php include('includes/html_header.php'); ?>

  <?php include('includes/template_navigation.php'); ?>

  <?php include('includes/header.php'); ?>

     <!--- CAROUSEL -->
<div class="ew-fullwidth cust-fullwidth carousel">
    <div class="container">
        <div class="row">

            <div id="carousel-example-generic" class="carousel ew-carousel cust-carousel slide" data-ride="carousel">

                <ol class="carousel-indicators ew-carousel-indicators cust-carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- carousel-indicators -->

                <div class="carousel-inner">

                    <div class="item active">
                        <img src="<?php echo $imagePath; ?>banner-01.jpg" alt="xxx" class="visible-md visible-lg">
                        <img src="<?php echo $imagePath; ?>banner-01.jpg" alt="xxx" class="visible-xs visible-sm">
                        <div class="carousel-caption">
                            <h1>Am Rande der Katastrophe</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                       <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default border">Lorem ipsum</a></p>
                        </div>
                        <!-- carousel-caption -->
                    </div>
                    <!-- item -->

                    <div class="item">
                        <img src="<?php echo $imagePath; ?>banner-02.jpg" alt="xxx" class="visible-md visible-lg">
                        <img src="<?php echo $imagePath; ?>banner-02.jpg" alt="xxx" class="visible-xs visible-sm">
                        <div class="carousel-caption">
                            <h1>Am Rande der Katastrophe</h1>
                            <p>Lorem ipsum ed do eiusmod tempor incididunt. Ipsum ed do eiusmod tempor incididunt</p>
                       <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default border">Lorem ipsum</a></p>
                        </div>
                        <!-- carousel-caption -->
                    </div>
                    <!-- item -->

                    <div class="item">
                        <img src="<?php echo $imagePath; ?>banner-01.jpg" alt="xxx" class="visible-md visible-lg">
                        <img src="<?php echo $imagePath; ?>banner-01.jpg" alt="xxx" class="visible-xs visible-sm">
                        <div class="carousel-caption">
                            <h1>Am Rande der Katastrophe</h1>
                            <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                       <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default border">Lorem ipsum</a></p>
                        </div>
                        <!-- carousel-caption -->
                    </div>
                    <!-- item -->

                </div>
                <!-- carousel-inner -->

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>

            </div>
            <!-- carousel-example-generic -->

        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div> <!-- ew-fullwidth cust-fullwidth carousel -->
    <!-- END CAROUSEL -->



    <!-- TEASER -->
<div class="ew-fullwidth cust-fullwidth teaser">
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-md-4">
            <div class="ew-front-teaser cust-front-teaser box">
                <h2>News</h2>
                <img src="<?php echo $imagePath; ?>news.jpg" alt="xxx" class="img-responsive">
                <h3>Lorem ipsum sit amet dolor adipiscing</h3>
                <p>in gravida nulla. Nulla vel metus scelern gravida nulla. Nulla vel  in gravida nisque ante Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio vestibulum.</p>
                <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default simple">weiter lesen</a>
                </p>
            </div> <!-- ew-front-teaser cust-front-teaser box -->
            </div>
            <!-- col-md-4 -->

            <div class="col-sm-6 col-md-4">
            <div class="ew-front-teaser cust-front-teaser box">
                <h2>Events</h2>
                <img src="<?php echo $imagePath; ?>events.jpg" alt="xxx" class="img-responsive">
                <h3>Lorem ipsum sit amet dolor adipiscing</h3>
                <p>Cras sit amet nibh libero, n gravida nulla. Nulla vel  in gravida nulla. Nulla vel metus Nulla vel metus scelerisque scelerisque ante sollicitudin commodo. Cras purus odio vestibulum.</p>
                <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default simple">weiter lesen</a>
                </p>
            </div> <!-- ew-front-teaser cust-front-teaser box -->
            </div>
            <!-- col-md-4 -->

 <!-- Add the extra clearfix for only the required viewport -->
    <div class="clearfix visible-sm"></div>

            <div class="col-sm-6 col-md-4">
            <div class="ew-front-teaser cust-front-teaser box">
                <h2>Projects</h2>
                <img src="<?php echo $imagePath; ?>projects.jpg" alt="xxx" class="img-responsive">
                <h3>Lorem ipsum sit amet dolor adipiscing</h3>
                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio vestibulum.</p>
                <p><a role="button" href="xxx" class="btn btn-default ew-btn-default cust-btn-default simple">weiter lesen</a>
                </p>
            </div> <!-- ew-front-teaser cust-front-teaser box -->
            </div>
            <!-- col-md-4 -->

        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div> <!-- ew-fullwidth cust-fullwidth teaser -->
    <!-- END TEASER -->



    <!-- JUMBOTRON -->
<div class="ew-fullwidth cust-fullwidth boilerplate">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <p class="text-center">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
            </div>
            <!-- jumbotron -->
            </div> <!-- col-md-12 -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div> <!-- ew-fullwidth cust-fullwidth boilerplate -->
    <!-- fullwidth -->
    <!-- END JUMBOTRON -->



  <?php include('includes/footer.php'); ?>

  <?php include('includes/js.php'); ?>

  </body>
</html>
